
export const days = 
[
  { value: "01", name: "01" },
  { value: "02", name: "02" },
  { value: "03", name: "03" },
  { value: "04", name: "04" },
  { value: "05", name: "05" },
  { value: "06", name: "06" },
  { value: "07", name: "07" },
  { value: "08", name: "08" },
  { value: "09", name: "09" },
  { value: "10", name: "10" },
  { value: "11", name: "11" },
  { value: "12", name: "12" },
  { value: "13", name: "13" },
  { value: "14", name: "14" },
  { value: "15", name: "15" },
  { value: "16", name: "16" },
  { value: "17", name: "17" },
  { value: "18", name: "18" },
  { value: "19", name: "19" },
  { value: "20", name: "20" },
  { value: "21", name: "21" },
  { value: "22", name: "22" },
  { value: "23", name: "23" },
  { value: "24", name: "24" },
  { value: "25", name: "25" },
  { value: "26", name: "26" },
  { value: "27", name: "27" },
  { value: "28", name: "28" },
  { value: "29", name: "29" },
  { value: "30", name: "30" },
  { value: "31", name: "31" },
];

export const months = [
  { value: "01", name: "January" },
  { value: "02", name: "February" },
  { value: "03", name: "March" },
  { value: "04", name: "April" },
  { value: "05", name: "May" },
  { value: "06", name: "June" },
  { value: "07", name: "July" },
  { value: "08", name: "August" },
  { value: "09", name: "September" },
  { value: "10", name: "October" },
  { value: "11", name: "November" },
  { value: "12", name: "December" },
]

export const years = [1920, 1021, 2022]


export const incomeRanges = [
  '900-2000',
  '2000-5000',
  '5000-10,000',
  'More then 10,000'
]


export const primaryDocumentTypes = [
  { id: 1, name: "Foreign Passport", point: 70 },
  { id: 2, name: "Australian Passport", point: 70 },
  { id: 3, name: "Australian Citizenship Certificate", point: 70 },
  { id: 4, name: "Full Birth certificate", point: 70 },
  { id: 5, name: "Certificate of Identity issued by the Australian Government to refugees and non Australian citizens forentry to Australia", point: 70 },
  { id: 6, name: "Australian Driver Licence/Learner’s Permit", point: 40 },
  { id: 7, name: "Current (Australian) Tertiary Student Identification Card", point: 40 },
  { id: 8, name: "Photo identification card issued for Australian regulatory purposes", point: 40 },
  { id: 9, name: "Government employee ID", point: 40 },
  { id: 10, name: "Defence Force Identity Card", point: 40 },
]

export const secondaryDocumentTypes = [
  { id: 1, name: "Department of Veterans Affairs (DVA) card", point: 40 },
  { id: 2, name: "Centrelink card (with reference number)", point: 40 },
  { id: 3, name: "Birth Certificate Extract", point: 25 },
  { id: 4, name: "Birth card (NSW Births, Deaths, Marriages issue only)", point: 25 },
  { id: 5, name: "Certificate of Identity issued by the Australian Government to refugees and non Australian citizens forentry to Australia", point: 70 },
  { id: 6, name: "Medivare card", point: 25 },
  { id: 7, name: "Credit card or account card", point: 25 },
  { id: 8, name: "Australian Marriage certificate (Australian Registry issue only)", point: 25 },
  { id: 9, name: "Decree Nisi / Decree Absolute (Australian Registry issue only)", point: 25 },
  { id: 10, name: "Change of name certificate (Australian Registry issue only)", point: 25 },
  { id: 11, name: "Bank statement (showing transactions)", point: 25 },
  { id: 12, name: "Property lease agreement - current address", point: 25 },
  { id: 13, name: "Taxation assessment notice", point: 25 },
  { id: 14, name: "Australian Mortgage Documents - Current address", point: 25 },
  { id: 15, name: "Rating Authority - Current address eg Land Rates", point: 25 },
  { id: 16, name: "Utility Bill - electricity, gas, telephone - Current address (less than 12 months old)", point: 20 },
  { id: 17, name: "Reference from Indigenous Organisation", point: 20 },
  { id: 18, name: "Documents issued outside Australia (equivalent to Australian documents).Must have official translation attached", point: 20 },
]