import axios from "axios";

const apiUrl = 'http://localhost:3900/api/v1/customers/send-email'

export function sendMail(data) {
  return axios.post(apiUrl, data)
}