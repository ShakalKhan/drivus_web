import React, { useState, useEffect } from 'react';
import { Form, Input, Progress, Select, Upload } from 'antd'
import { Button, Image, Loading } from "@nextui-org/react";
import { UploadOutlined } from '@ant-design/icons';
import { MdOutlineArrowBackIos } from "react-icons/md";
import { saveApplication } from '../../service/customer.service';

const StepDrivingLicense = ({ current, data, setData, nextStep }) => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    const newData = { ...data.drivingLicense };
    // console.log("Current", current, data.drivingLicense);
    if (data.drivingLicense?.filePath) {
      newData.fileList = [{ uid: 1, name: data.drivingLicense.filePath, url: `http://localhost:5000/uploads/${data.drivingLicense.filePath}` }]
    }
    form.setFieldsValue(newData);
  }, [current, data.id]);

  const normFile = (e) => {
    if (Array.isArray(e)) return e;
    const filePath = e.file?.response?.file[0]?.filename;
    // console.log("EEEEEE", filePath, e.file);
    form.setFieldsValue({ filePath })
    return e && e.fileList;
  };

  
  const handleFinish = async values => {

    const newData = { drivingLicense: { type: values.type, filePath: values.filePath } }
    // return console.log(values);

    try {
      if (isSameData(values)) {
        return nextStep();
      }
      setLoading(true);
      setTimeout(async () => {
        await saveApplication(data.id, newData);
        setLoading(false);
        setData({ ...data, ...newData });
        nextStep()
      }, 300);
    } catch (err) {
      setLoading(false);
      if (err.response?.data) {
        console.log(err.response.data.errors);
      }
    }
  }


  const isSameData = values => {
    const { type, filePath } = data.drivingLicense || {};
    if (type !== values.type || filePath !== values.filePath) return false;
    return true;
  }

  return (
    <div className='container'>
      <div className="row" style={{ minHeight: "650px" }}>
        <div className="col-md-7">
          <Form
            form={form}
            autoComplete="off"
            onFinish={handleFinish}
          >
            {values => (
              <div className="row">
                <div className="col-12 mt-5 mb-4 pt-lg-5">
                  <h2>What Kind of Driving License you have?</h2>
                </div>
                <div className="col-md-5">
                  <Form.Item
                    name="type" //{['drivingLicense', 'type']}
                    rules={[
                      {
                        required: true,
                        message: "Driving License Type is required"
                      },
                    ]}
                  >
                    <Select
                      size='large'
                      allowClear showArrow
                      placeholder='Type of Driving license'
                      optionFilterProp='children'
                      filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                    >
                      <Select.Option value="Rider">Rider</Select.Option>
                      <Select.Option value="Full licence">Full licence</Select.Option>
                      <Select.Option value="Learners licence">Learners licence</Select.Option>
                      <Select.Option value="Provisional licence stage 1 (P1)">Provisional licence stage 1 (P1)</Select.Option>
                      <Select.Option value="Provisional licence stage 2 (P2)">Provisional licence stage 2 (P2)</Select.Option>
                    </Select>
                  </Form.Item>

                  <Form.Item
                    name="filePath"
                    hidden
                  >
                    <Input />
                  </Form.Item>

                </div>
                <div className="col-md-7">
                  <Form.Item
                    name="fileList"
                    valuePropName="fileList"
                    getValueFromEvent={normFile}
                    rules={[
                      {
                        required: true,
                        message: "File is required"
                      },
                    ]}
                  >
                    <Upload
                      name='file'
                      // fileList={formData.drivingLicense ? (formData.drivingLicense.filePath||[]) : []}
                      // listType='picture'
                      maxCount={1}
                      beforeUpload={() => true}
                      action={`http://localhost:5000/api/v1/files/uploads`}
                    >
                      {/* {(!form.getFieldValue('drivingLicense')?.fileList||form.getFieldValue('drivingLicense')?.fileList?.length<=0) && 
                        <Button
                          bordered 
                          auto 
                          color="secondary" 
                          size="sm" 
                          icon={<UploadOutlined />}
                        >Upload</Button>} */}
                      {(values.fileList || []).length < 1 &&
                        <Button
                          bordered
                          auto
                          color="secondary"
                          size="sm"
                          icon={<UploadOutlined />}
                        > Upload</Button>}
                    </Upload>
                  </Form.Item>
                </div>

                <div className="col-12 d-flex mt-5">
                  <Button color="secondary" bordered auto onClick={nextStep}>{`Skip >>`}</Button>
                  <Button
                    auto
                    className='ms-2'
                    color="secondary"
                    type="submit"
                  >{isSameData(values) ? 'Continue' : 'Save & Continue'}</Button>
                  {loading && <Loading className='m-2' type="spinner" />}
                </div>
              </div>
            )}
          </Form>

        </div>
        <div className="col-md-5">
          <Image
            src="/img/driving-license.png"
            alt="Driving License"
            width={700}
            height={500}
          />
        </div>

      </div>

      <div className='mt-5'>
        <Progress
          strokeColor={{
            from: '#108ee9',
            to: '#87d068',
          }}
          percent={30}
          status="active"
        />
      </div>
    </div>
  )
}

export default StepDrivingLicense