import React, { useState, useEffect } from 'react';
import { Form, Progress, Radio, Space } from 'antd';
import { Button, Image, Loading } from "@nextui-org/react";
import { saveApplication } from '../../service/customer.service';

const StepCarCategory = ({ current, data, setData, nextStep }) => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    form.setFieldsValue({ desiredCar: data.desiredCar })
  }, [current, data.id]);

  const handleFinish = async (values) => {
    const newData = { desiredCar: values.desiredCar };

    // return console.log("data", values);

    try {
      if (data.desiredCar === newData.desiredCar) return nextStep();
      setLoading(true);
      setTimeout(async () => {
        await saveApplication(data.id, newData);
        setLoading(false);
        setData({ ...data, ...newData });
        nextStep()
      }, 300);
    } catch (err) {
      setLoading(false);
      if (err.response?.data) {
        console.log(err.response.data.errors);
      }
    }
  }


  const carImages = {
    "Base model Sedan": "sedan1.png",
    "Mid range Sedan": "sedan2.jpg",
    "Top range Sedan": "top-range-sedan.jpg",
    "Suv": "suv1.png",
    "Premium car": "premium-car.jpg",
    "7-7+ Seater": "seater7.jpg",
    "UTE": "audi.png",
  }

  return (
    <div className='container'>
      <div className='row' style={{ minHeight: "650px" }}>
        <div className='col-md-7'>

          <Form
            form={form}
            autoComplete="off"
            onFinish={handleFinish}
          >
            {values => (
              <div className="row mt-5">
                <div className='col-12 pt-lg-5'>
                  <h2>Please choose from the categories below of <br /> your desired car.</h2>
                </div>
                <div className="col-12 my-4">
                  <Form.Item
                    name="desiredCar"
                    rules={[
                      {
                        required: true,
                        message: 'Please choose your desired car',
                      },
                    ]}
                  >
                    <Radio.Group>
                      <Space direction="vertical">
                        <Radio value="Base model Sedan">Base model Sedan, Rent - 275 AUD/week</Radio>
                        <Radio value="Mid range Sedan">Mid range Sedan, Rent - 300 AUD/Week</Radio>
                        <Radio value="Top range Sedan">Top range Sedan, Rent - 325 AUD/Week</Radio>
                        <Radio value="Suv">Suv, Rent - 350 AUD/week</Radio>
                        <Radio value="Premium car">Premium car, Rent - 375-400 AUD/week</Radio>
                        <Radio value="7-7+ Seater">7-7+ Seater, Rent - 375-425 AUD/week</Radio>
                        <Radio value="UTE">UTE -Rent - 375-450 AUD/week</Radio>
                      </Space>
                    </Radio.Group>
                  </Form.Item>
                </div>
                <div className="col-12 d-flex">
                  <Button color="secondary" bordered auto onClick={nextStep}>{`Skip >>`}</Button>
                  <Button
                    auto
                    className='ms-2'
                    color="secondary"
                    type="submit"
                  >{((!data.desiredCar && !values.desiredCar) || values.desiredCar === data.desiredCar) ? 'Continue' : 'Save & Continue'}</Button>
                  {loading && <Loading className='m-2' type="spinner" />}
                </div>

              </div>
            )}
          </Form>
        </div>
        <div className='col-md-5'>
          <div className='mt-5'>
            <Image
              src={`/img/car/${carImages[form.getFieldValue('desiredCar')] || 'cars.jpg'}`}
              alt="Driving License"
              width={700}
            />
          </div>
        </div>
      </div>

      <Progress
        strokeColor={{
          from: '#108ee9',
          to: '#87d068',
        }}
        percent={70}
        status="active"
      />

    </div>
  )
}

export default StepCarCategory;
