import React from 'react'
import styles from '../styles/Testimonial.module.css'
import 'animate.css';
import Header from '../components/Header';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';

const HowItWorks = () => {
  return (
    <div className=''>
      <Header />
      <Navbar />
      <div className={`${styles.how} animate__animated animate__fadeIn animate__slow`}>
        <div className='container-fluid' style={{ height: 660 }}>

          <div className='row'>

            <div className='col-md-1'></div>

            <div className='col-md-4 mt-3 pt-4 ms-0 ms-md-4 animate__animated animate__fadeInUp animate__delay-2s'>
              <h3 className='fw-bold' style={{ fontSize: 27, color: "#272561" }}>How it works</h3>
              <h1 className='fw-bold mt-3' style={{ color: "#272561", fontSize: 40 }}>Drivus support from the starting line <br /><span className='text-danger'>own their car.</span></h1>
            </div>

            <div className='col-md-5'></div>

          </div>
        </div>
      </div>
      <div className='bg-light mt-5'>

        <div className='container animate__animated animate__fadeInUp animate__delay-2s'>
          <iframe className='my-5 pb-2' style={{ width: "100%", height: "590px" }} src="https://www.youtube.com/embed/b6FRcAHJYNI" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
        </div>
      </div>
      <div className='container mt-3' style={{ color: "#25225B" }}>
        <h1 className='fw-bold pt-5' style={{ fontSize: 34, color: "#272561" }}>Why Drivus?</h1>
        <div className='row'>

          <div className='col-md-6'>
            <p className='mt-3' style={{ fontSize: 17 }}>Build your career fast, not furious. Get a car, plus the training and support you need to become a more profitable, safer, fulfilled driver. We will help you manage
              the business side of things, so you can do what you do best. Drive
            </p>
          </div>

        </div>

        <div className='row mb-5 pb-5'>

          <div className='col-md-6'>
            <div className='p-4 shadow-lg ' style={{ backgroundColor: "#EDEDED", borderRadius: 20, minHeight: 360 }}>
              <h1 className='fw-bold' style={{ color: "#272561", fontSize: 34 }}>What documents do I need for signing up?</h1>
              <div className='mt-4' style={{ fontSize: 17, color: "#2522B" }}>-- <span className='ms-2'>Drivers license</span></div>
              <div style={{ fontSize: 17, color: "#2522B" }}>-- <span className='ms-2'>Proof of address</span></div>
              <div style={{ fontSize: 17, color: "#2522B" }}>-- <span className='ms-2'>Your passport or citizenship certificate</span></div>
              <div style={{ fontSize: 17, color: "#2522B" }}>-- <span className='ms-2'>Driving history</span></div>
            </div>
          </div>

          <div className='col-md-6 mt-md-0 mt-3'>
            <div className='p-4 shadow-lg' style={{ backgroundColor: "#B8BFFF", borderRadius: 20, minHeight: 360 }}>
              <h1 className='fw-bold' style={{ color: "#272561", fontSize: 34 }}>How much does it cost to get started?</h1>

              <div className='mt-4' style={{ fontSize: 17, color: "#2522B" }}>
                New members pay a $275 joining fee with weekly car payments starting from $188 on a Flexi subscription, and a $990 joining fee with cars starting from $329 on Flexi own.
              </div>

              <div className='mt-4' style={{ fontSize: 17, color: "#2522B" }}>Our weekly plans run Monday-Sunday. When collecting your car, you will pay one full week in advance, plus remaining days of
                the current week including collection day. So, if you collect
                your car on a Friday, for example, we will charge you one full
                week covering next weeks subscription, plus daily rates for
                Friday to Sunday.  <span className="text-primary" style={{ cursor: "pointer" }}>Learn more about our start-up costs</span>
              </div>

            </div>
          </div>

        </div>

      </div>

      <Footer />
    </div>

  )
}

export default HowItWorks