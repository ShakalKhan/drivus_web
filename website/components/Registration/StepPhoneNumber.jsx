import React, { useState, useEffect } from 'react';
import { Form, Progress } from 'antd'
import { Button, Image, Loading, Input } from "@nextui-org/react";
import { saveApplication } from '../../service/customer.service';

const StepPhoneNumber = ({ current, data, setData, nextStep }) => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    form.setFieldsValue({ phone: data.phone });
  }, [current, data.id]);


  const handleFinish = async values => {

    const newData = { phone: values.phone };
    // return console.log(values);

    if (data.phone === newData.phone) return nextStep();

    try {
      if (data.phone === phone) return nextStep();
      setLoading(true);
      setTimeout(async () => {
        await saveApplication(data.id, newData);
        setLoading(false);
        setData({ ...data, ...newData });
        nextStep()
      }, 300);
    } catch (err) {
      setLoading(false);
      if (err.response?.data) {
        console.log(err.response.data.errors);
      }
    }
  }


  return (
    <div className='container'>
      <div className="row" style={{ minHeight: "650px" }}>
        <div className="col-md-7">
          <Form
            form={form}
            autoComplete="off"
            onFinish={handleFinish}
          >
            {values => (

              <div className="row">
                <div className="col-12 my-5 pt-lg-5">
                  <h2>Provide your phone number</h2>
                </div>
                <div className="col-md-12">
                  <Form.Item
                    name="phone"
                    rules={[
                      {
                        required: true,
                        message: "Phone Number is required"
                      },
                      {
                        pattern: /^(?:\+88|01)?(?:\d{11}|\d{13})$/i,
                        message: 'Wrong format!',
                      },
                    ]}
                  >
                    <Input
                      underlined
                      labelPlaceholder='Phone No.'
                      color="primary"
                      style={{ width: "400px" }}
                    />
                  </Form.Item>
                </div>

                <div className="col-12 d-flex mt-5">
                  <Button color="secondary" bordered auto onClick={nextStep}>{`Skip >>`}</Button>
                  <Button
                    auto
                    className='ms-2'
                    color="secondary"
                    type="submit"
                  >{((!data.phone && !values.phone) || values.phone === data.phone) ? 'Continue' : 'Save & Continue'}</Button>
                  {loading && <Loading className='m-2' type="spinner" />}
                </div>
              </div>
            )}
          </Form>

        </div>
        <div className="col-md-5">
          <Image
            src="/img/phone-number.svg"
            alt="Driving License"
            width={700}
            height={500}
          />
        </div>

      </div>

      <Progress
        strokeColor={{
          from: '#108ee9',
          to: '#87d068',
        }}
        percent={100}
        status="active"
      />
    </div>
  )
}

export default StepPhoneNumber