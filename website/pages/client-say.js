import styles from "../styles/Testimonial.module.css"
import { Carousel } from 'antd';

const ClientSay = () => {
  return (
    <div className={styles.pic}>
      <div className={styles.blur}>

        <div className="container pt-5 pb-4 text-white" style={{ minHeight: 550 }}>
          <div className="row mt-2 mt-md-5 px-3 px-md-0">
            <div className="col-md-6 mt-4">
              <h1 className=" fw-bold text-white mb-3 mt-4" style={{ fontSize: 40 }}>What our clients say?</h1>
              <Carousel autoplay className="text-white">
                <div>
                  <p className="fw-bold my-4" style={{ fontSize: 20 }}>Steven P. - University Student</p>
                  <p className="pb-5 mb-5" style={{ fontSize: 17 }}>Drivus has been awesome.The cars are great and the support from the drivus representatives is fantastic. They do go all out to make sure you feel the freedom of your choice.</p>
                </div>
                <div>
                  <p className="fw-bold my-4" style={{ fontSize: 20 }}>Rafi. - University Student</p>
                  <p className="pb-5 mb-5" style={{ fontSize: 17 }}>Drivus has been awesome.The cars are great and the support from the drivus representatives is fantastic. They do go all out to make sure you feel the freedom of your choice.</p>
                </div>
                <div>
                  <p className="fw-bold my-4" style={{ fontSize: 20 }}>Shakal. - University Student</p>
                  <p className="pb-5 mb-5" style={{ fontSize: 17 }}>Drivus has been awesome.The cars are great and the support from the drivus representatives is fantastic. They do go all out to make sure you feel the freedom of your choice.</p>
                </div>
                <div>
                  <p className="fw-bold my-4" style={{ fontSize: 20 }}>Goutam. - University Student</p>
                  <p className="pb-5 mb-5" style={{ fontSize: 17 }}>Drivus has been awesome.The cars are great and the support from the drivus representatives is fantastic. They do go all out to make sure you feel the freedom of your choice.</p>
                </div>
              </Carousel>
            </div>
          </div>
        </div>

      </div>
    </div>

  )
}

export default ClientSay;