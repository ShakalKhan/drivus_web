import React, { useState, useEffect } from 'react';
import { Checkbox, Progress } from 'antd'
import { Button, Image, Loading, Modal, Text } from "@nextui-org/react";
import { saveApplication } from '../../service/customer.service';

const StepSubmission = ({ current, data, setData }) => {
  const [totalPoint, setTotalPoint] = useState(230);
  const [totalIncome, setIncome] = useState(470);
  const [loading, setLoading] = useState(false);
  const [submitted, setSubmitted] = useState(false);
  const [checked, setChecked] = useState(false);


  const handleSubmit = async () => {

    const newData = { status: "Application Completed" }
    // return console.log(newData);

    try {
      setLoading(true);
      setTimeout(async () => {
        await saveApplication(data.id, newData);
        setLoading(false);
        setSubmitted(true)
        setData({ ...data, ...newData });
      }, 300);
    } catch (err) {
      setLoading(false);
      if (err.response?.data) {
        console.log(err.response.data.errors);
      }
    }
  }


  return (
    <div className='container'>
      <div className="row" style={{ minHeight: "650px" }}>
        <div className="col-md-7 mt-5">
          <h2>Review your data and Submit</h2>

          <div className='row'>
            <div className="col-md-12 mb-3">
              <table>
                <tbody>
                  <tr><td style={{ width: '110px' }}>First name</td><td>: <span className='fw-bold'>{data.firstName}</span></td></tr>
                  <tr><td>Last name</td><td>: <span className='fw-bold'>{data.lastName}</span></td></tr>
                  <tr><td>Email</td><td>: <span className='fw-bold'>{data.email}</span></td></tr>
                  <tr><td>Phone</td><td>: <span className='fw-bold'>{data.phone}</span></td></tr>
                </tbody>
              </table>
            </div>
            <div className="col-md-12 my-3">
              <h4>Citizenship</h4>
              {data.citizenship}
              {data.residency && <p>Residency: <span className='fw-bold'>{data.residency}</span></p>}
            </div>

            <div className='col-md-12 my-3 text-dark'>
              <Checkbox checked disabled className='me-2' />
              I am agreed with Drivus, that i do not intend to use any of Drivus vehicles for any kind of ride sharing app.
            </div>

            <div className="col-md-12 my-3">
              <h4>Driving License</h4>
              {data.drivingLicense ? <table>
                <tbody>
                  <tr><td style={{ width: '110px' }}>License Type</td><td>: <span className='fw-bold'>{data.drivingLicense.type}</span></td></tr>
                  <tr><td>Document Status</td><td>: <span className='fw-bold'>{data.drivingLicense.filePath ? "Uploaded" : "No Uploaded"}</span></td></tr>
                </tbody>
              </table> :
                <p className='text-danger'>Please provide your driving license data</p>
              }
            </div>

            <div className="col-md-12 my-3">
              <h4>100 point Id Documents</h4>

              <p>Total Point: {totalPoint < 100 ? <span className='text-danger h5'>{totalPoint}</span> : <span className='text-success h5'>{totalPoint}</span>}</p>

              <h6>Primary Documents</h6>
              {data.primaryDocuments ? (
                <table>
                  <tbody>
                    {
                      data.primaryDocuments.map((doc, idx) => (
                        <tr key={'primary_' + idx}>
                          <td style={{ width: "30px" }}><Checkbox checked={doc.filePath} disabled /></td>
                          <td>{doc.type}</td>
                        </tr>
                      ))
                    }
                  </tbody>
                </table>
              ) :
                <p className='text-danger'>Please provide at least 1 primary documents</p>
              }

              <h6 className='mt-3'>Secondary Documents</h6>
              {data.secondaryDocuments ? (
                <table>
                  <tbody>
                    {
                      data.secondaryDocuments.map((doc, idx) => (
                        <tr key={'ss_' + idx}>
                          <td style={{ width: "30px" }}><Checkbox checked={doc.filePath} disabled /></td>
                          <td>{doc.type}</td>
                        </tr>
                      ))
                    }
                  </tbody>
                </table>
              ) :
                <p className='text-danger'>Please provide at least 1 secodery documents</p>
              }
            </div>

            <div className="col-md-12 my-3">
              <h4 className='mt-3'>Income Sources</h4>
              {totalIncome < 700 && <p className='text-danger'>Your total income at least $ 700 /week</p>}
              {data.incomeSources ? (
                <table>
                  <tbody>
                    {
                      data.incomeSources.map((doc, idx) => (
                        <tr key={'is_' + idx}>
                          <td style={{ width: "30px" }}><Checkbox checked={doc.filePath} disabled /></td>
                          <td style={{ width: "110px" }}>{doc.name}</td>
                          <td>$ {doc.amount} /week</td>
                        </tr>
                      ))
                    }
                  </tbody>
                </table>
              ) :
                <p className='text-danger'>Please provide your sources of income</p>
              }
            </div>

            <div className="col-md-12 my-3">
              <h4>My Desired car</h4>
              {data.desiredCar}
            </div>

            <div className="col-md-6 my-3">
              <h4>Postal Address</h4>
              {data.postalAddress ? <table>
                <tbody>
                  <tr><td style={{ width: '110px' }}>Street</td><td>: <span className='fw-bold'>{data.postalAddress.street}</span></td></tr>
                  <tr><td>Street</td><td>: <span className='fw-bold'>{data.postalAddress.suburb}</span></td></tr>
                  <tr><td>State</td><td>: <span className='fw-bold'>{data.postalAddress.state}</span></td></tr>
                  <tr><td>Post Code</td><td>: <span className='fw-bold'>{data.postalAddress.postCode}</span></td></tr>
                  <tr><td>Document Type</td><td>: <span className='fw-bold'>{data.postalAddress.documentType}</span></td></tr>
                  <tr><td>Document Status</td><td>: <span className='fw-bold'>{data.postalAddress.filePath ? "Uploaded" : "No Uploaded"}</span></td></tr>
                </tbody>
              </table> :
                <p className='text-danger'>Please provide your Postal address</p>
              }
            </div>

            <div className="col-md-6 my-3">
              <h4>Residential Address</h4>
              {data.residentialAddress ? <table>
                <tbody>
                  <tr><td style={{ width: '110px' }}>Street</td><td>: <span className='fw-bold'>{data.residentialAddress.street}</span></td></tr>
                  <tr><td>Street</td><td>: <span className='fw-bold'>{data.residentialAddress.suburb}</span></td></tr>
                  <tr><td>State</td><td>: <span className='fw-bold'>{data.residentialAddress.state}</span></td></tr>
                  <tr><td>Post Code</td><td>: <span className='fw-bold'>{data.residentialAddress.postCode}</span></td></tr>
                  <tr><td>Document Type</td><td>: <span className='fw-bold'>{data.residentialAddress.documentType}</span></td></tr>
                  <tr><td>Document Status</td><td>: <span className='fw-bold'>{data.residentialAddress.filePath ? "Uploaded" : "No Uploaded"}</span></td></tr>
                </tbody>
              </table> :
                <p className='text-danger'>Please residential your Postal address</p>
              }
            </div>


            <div className='col-md-12'>
              <hr />
            </div>
            <div className="col-9">
              <div>
                <Checkbox
                  checked={checked}
                  className='me-2'
                  onClick={() => setChecked(!checked)}
                >I've aggreed with Drivus terms and policies</Checkbox>
              </div>
            </div>
            <div className='col-3 d-flex mb-lg-5'>
              <Button
                auto
                size="sm"
                color="secondary"
                onClick={handleSubmit}
                disabled={!checked}
              >Submit</Button>
              {loading && <Loading className='m-2' type="spinner" />}
            </div>
          </div>

        </div>

        <div className="col-md-5">
          <Image
            src="/img/submission.jpg"
            alt="Driving License"
            width={700}
            height={500}
          />
        </div>

      </div>

      <div className='pb-5 '>
        <Progress
          strokeColor={{
            from: '#108ee9',
            to: '#87d068',
          }}
          percent={100}
          status="active"
        />
      </div>

      <Modal
        aria-labelledby="modal-title"
        noPadding
        open={submitted}
        onClose={() => setSubmitted(false)}
        closeButton
        preventClose
      >
        <Modal.Header
        >
          <h4>Data Submitted</h4>
        </Modal.Header>
        <Modal.Body>
          <Image
            showSkeleton
            src="/img/submitted.png"
            width={400}
            height={300}
          />
        </Modal.Body>
      </Modal>
    </div>
  )
}

export default StepSubmission