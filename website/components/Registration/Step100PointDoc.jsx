import React, { useState, useEffect } from 'react';
import { Form, Input, Progress, Select, Space, Upload, Button as AntButton } from 'antd'
import { Button, Image, Loading } from "@nextui-org/react";
import { UploadOutlined, PlusOutlined, MinusOutlined, MinusCircleOutlined } from '@ant-design/icons';
import { saveApplication } from '../../service/customer.service';
import { primaryDocumentTypes, secondaryDocumentTypes } from '../../utils/array';

const Step100PointDoc = ({ current, data, setData, nextStep }) => {
  const [form] = Form.useForm();
  const [formState, setFormState] = useState({})
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    const primaryDocuments = (data.primaryDocuments || [{}]).map((item, idx) => {
      const newItem = {};
      if (item.type) {
        newItem.preType = item.type;
        newItem.type = item.type;
      }
      if (item.filePath) {
        newItem.preFilePath = item.filePath;
        newItem.filePath = item.filePath;
        newItem.fileList = [{ uid: `primary_${idx}`, name: item.filePath, url: `http://localhost:5000/uploads/${item.filePath}` }]
      }
      return newItem;
    });

    const secondaryDocuments = (data.secondaryDocuments || [{}]).map((item, idx) => {
      const newItem = {};
      if (item.type) {
        newItem.preType = item.type;
        newItem.type = item.type;
      }
      if (item.filePath) {
        newItem.preFilePath = item.filePath;
        newItem.filePath = item.filePath;
        newItem.fileList = [{ uid: `secondary_${idx}`, name: item.filePath, url: `http://localhost:5000/uploads/${item.filePath}` }]
      }

      return newItem;
    });

    form.setFieldsValue({ primaryDocuments, secondaryDocuments });
  }, [current, data.id]);


  const normFile = (e, arrName, key) => {
    if (Array.isArray(e)) return e;
    const filePath = e.file?.response?.file && e.file.response.file[0]?.filename;
    // console.log("EEEEEE", filePath, key, e.file);
    const documents = [...form.getFieldValue(arrName)];
    if (documents[key]) {
      documents[key].filePath = filePath;
    }
    form.setFieldsValue({ [arrName]: documents })
    return e && e.fileList;
  };

  const handleFinish = async values => {
    const newData = {};

    const modified = isModified(values);
    if (!modified) return nextStep();

    if (modified.primary) {
      newData.primaryDocuments = values.primaryDocuments.map(({ type, filePath }) => ({ type, filePath }));
    }
    if (modified.secondary) {
      newData.secondaryDocuments = values.secondaryDocuments.map(({ type, filePath }) => ({ type, filePath }));
    }

    // return console.log(modified, newData);

    try {
      setLoading(true);
      setTimeout(async () => {
        await saveApplication(data.id, newData);
        setLoading(false);
        setData({ ...data, ...newData });
        nextStep()
      }, 300);
    } catch (err) {
      setLoading(false);
      if (err.response?.data) {
        console.log(err.response.data.errors);
      }
    }
  }


  const isModified = values => {
    // thik korte hobe
    let primary = false, secondary = false;
    if (data.primaryDocuments?.length !== values.primaryDocuments?.length) primary = true;
    if (data.secondaryDocuments?.length !== values.secondaryDocuments?.length) secondary = true;

    if (!primary && !secondary) {
      values.primaryDocuments?.forEach(({ type, preType, filePath, preFilePath }) => {
        if (type !== preType || filePath !== preFilePath) return primary = true;
      })
      values.secondaryDocuments?.forEach(({ type, preType, filePath, preFilePath }) => {
        if (type !== preType || filePath !== preFilePath) return secondary = true;
      })
    }

    return (primary || secondary) ? ({ primary, secondary }) : null;
  }

  const getTotalPoint = values => {
    let documents = [];
    if (values.primaryDocuments) documents = [...values.primaryDocuments]
    if (values.secondaryDocuments) documents = [...documents, ...values.secondaryDocuments]
    return documents.reduce((acc, curr) => acc + (documentType[curr.type] || 0), 0);
  }


  const documentType = [...primaryDocumentTypes, ...secondaryDocumentTypes].reduce((acc, cur) => ({ ...acc, [cur.name]: cur.point }), {})


  return (
    <div className='container'>
      <div className="row" style={{ minHeight: "650px" }}>
        <div className="col-md-7">
          <Form
            form={form}
            name="formData"
            autoComplete="off"
            onFinish={handleFinish}
          >
            {
              (values) => (
                <div className="row">
                  <div className="col-12 my-3">
                    <h2>Provide your 100 points Id</h2>
                  </div>

                  <div className="col-md-12">
                    <h6 className='text-primary'>Primary Documents</h6>
                    <Form.List name="primaryDocuments">
                      {(fields, { add, remove }) => (
                        <>
                          {fields.map(({ key, name }) => (
                            <div className='row' key={key}>
                              <div className="col-6">
                                <Form.Item
                                  name={[name, 'type']}
                                  rules={[{ required: true, message: 'Type is required' }]}
                                >
                                  <Select
                                    allowClear
                                    showSearch
                                    style={{ width: '100%' }}
                                    placeholder='Select Document Type'
                                    optionFilterProp='children'
                                    filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                  >
                                    {primaryDocumentTypes.map(item =>
                                      <Select.Option key={item.id} value={item.name}>{item.name}</Select.Option>
                                    )}
                                  </Select>
                                </Form.Item>
                              </div>

                              <div className="col-1">
                                <div>{documentType[values.primaryDocuments[name]?.type]}</div>

                                <Form.Item
                                  name={[name, 'preType']}
                                  hidden
                                >
                                  <Input />
                                </Form.Item>

                                <Form.Item
                                  name={[name, 'filePath']}
                                  hidden
                                >
                                  <Input />
                                </Form.Item>

                                <Form.Item
                                  name={[name, 'preFilePath']}
                                  hidden
                                >
                                  <Input />
                                </Form.Item>
                              </div>

                              <div className="col-4">
                                <Form.Item
                                  name={[name, 'fileList']}
                                  valuePropName='fileList'
                                  getValueFromEvent={(e) => normFile(e, 'primaryDocuments', name)}
                                  rules={[{ required: true, message: "File is required" }]}
                                >
                                  <Upload
                                    name='file'
                                    // fileList={formData.drivingLicense ? (formData.drivingLicense.filePath||[]) : []}
                                    // listType='picture'
                                    maxCount={1}
                                    beforeUpload={() => true}
                                    action={`http://localhost:5000/api/v1/files/uploads`}
                                  >
                                    {/* {(!form.getFieldValue('drivingLicense')?.fileList||form.getFieldValue('drivingLicense')?.fileList?.length<=0) && 
                                      <Button
                                        bordered 
                                        auto 
                                        color="secondary" 
                                        size="sm" 
                                        icon={<UploadOutlined />}
                                      >Upload</Button>} */}
                                    {(values.primaryDocuments[name]?.fileList || []).length < 1 &&
                                      <AntButton icon={<UploadOutlined />}>Upload</AntButton>
                                    }
                                  </Upload>
                                </Form.Item>
                              </div>
                              <div className="col-1">
                                {
                                  values.primaryDocuments.length > 1 ?
                                    <MinusCircleOutlined className='delete' onClick={() => remove(name)} />
                                    : <MinusCircleOutlined className='text-muted' />
                                }
                              </div>
                            </div>
                          ))}
                          <Form.Item>
                            <Button
                              color=""
                              size="sm"
                              auto
                              bordered
                              onClick={() => add({})}
                            ><PlusOutlined className='me-2' /> Add</Button>
                          </Form.Item>
                        </>
                      )}
                    </Form.List>
                  </div>

                  <div className="col-md-12">
                    <h6 className='text-primary'>Secondary Documents</h6>
                    <Form.List name="secondaryDocuments">
                      {(fields, { add, remove }) => (
                        <>
                          {fields.map(({ key, name }) => (
                            <div className='row' key={key}>
                              <div className="col-6">
                                <Form.Item
                                  name={[name, 'type']}
                                  rules={[{ required: true, message: 'Type is required' }]}
                                >
                                  <Select
                                    allowClear
                                    showSearch
                                    style={{ width: '100%' }}
                                    placeholder='Select Document Type'
                                    optionFilterProp='children'
                                    filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}
                                  >
                                    {secondaryDocumentTypes.map(item =>
                                      <Select.Option key={item.id} value={item.name}>{item.name}</Select.Option>
                                    )}
                                  </Select>
                                </Form.Item>
                              </div>

                              <div className="col-1">
                                {documentType[values.secondaryDocuments[name]?.type]}

                                <Form.Item
                                  name={[name, 'preType']}
                                  hidden
                                >
                                  <Input />
                                </Form.Item>

                                <Form.Item
                                  name={[name, 'filePath']}
                                  hidden
                                >
                                  <Input />
                                </Form.Item>

                                <Form.Item
                                  name={[name, 'preFilePath']}
                                  hidden
                                >
                                  <Input />
                                </Form.Item>
                              </div>

                              <div className="col-4">
                                <Form.Item
                                  name={[name, 'fileList']}
                                  valuePropName="fileList"
                                  getValueFromEvent={(e) => normFile(e, 'secondaryDocuments', name)}
                                  rules={[{ required: true, message: "File is required" }]}
                                >
                                  <Upload
                                    name='file'
                                    // fileList={formData.drivingLicense ? (formData.drivingLicense.filePath||[]) : []}
                                    // listType='picture'
                                    maxCount={1}
                                    beforeUpload={() => true}
                                    action={`http://localhost:5000/api/v1/files/uploads`}
                                  >
                                    {/* {(!form.getFieldValue('drivingLicense')?.fileList||form.getFieldValue('drivingLicense')?.fileList?.length<=0) && 
                                      <Button
                                        bordered 
                                        auto 
                                        color="secondary" 
                                        size="sm" 
                                        icon={<UploadOutlined />}
                                      >Upload</Button>} */}
                                    {(values.secondaryDocuments[name]?.fileList || []).length < 1 &&
                                      <AntButton icon={<UploadOutlined />}>Upload</AntButton>
                                    }
                                  </Upload>
                                </Form.Item>
                              </div>
                              <div className="col-1">
                                {
                                  values.secondaryDocuments.length > 1 ?
                                    <MinusCircleOutlined className='delete' onClick={() => remove(name)} />
                                    : <MinusCircleOutlined className='text-muted' />
                                }
                              </div>
                            </div>
                          ))}
                          <Form.Item>
                            <Button
                              color=""
                              size="sm"
                              auto
                              bordered
                              onClick={() => add({})}
                            ><PlusOutlined className='me-2' /> Add</Button>
                          </Form.Item>
                        </>
                      )}
                    </Form.List>
                  </div>


                  <div className="col-8 d-flex my-5">
                    <Button color="secondary" bordered auto onClick={nextStep}>{`Skip >>`}</Button>
                    <Button
                      auto
                      className='ms-2'
                      color="secondary"
                      type="submit"
                    >{isModified(values) ? 'Save & Continue' : 'Continue'}</Button>
                    {loading && <Loading className='m-2' type="spinner" />}
                  </div>
                  {/* <div className="col-md-4 mt-4 text-end">
                    <Progress 
                      size='small'
                      type="circle" 
                      percent={100} 
                      format={() => <span>320</span>}
                      width={80}
                    />
                  </div> */}
                </div>
              )
            }
          </Form>

        </div>
        <div className="col-md-5">
          <div className="row mt-lg-5 pt-lg-5">
            <div className="col-md-7 mt-lg-5 pt-lg-5 text-center">
              <Progress
                size='small'
                type="circle"
                strokeColor={{
                  '0%': '#108ee9',
                  '100%': '#87d068',
                }}
                percent={getTotalPoint(form.getFieldsValue())}
                format={() => getTotalPoint(form.getFieldsValue())}
              />
              <h6 className='mt-3'>Total Point of Docuemnts</h6>
            </div>
            <div className="col-md-5 ">
              <Image
                src="/img/documents.png"
                alt="Driving License"
                width={500}
                height={300}
              />
            </div>
          </div>
        </div>

      </div>

      <Progress
        strokeColor={{
          from: '#108ee9',
          to: '#87d068',
        }}
        percent={50}
        status="active"
        className='mb-5'
      />
    </div>
  )
}

export default Step100PointDoc