import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router'

import jwtDecode from "jwt-decode";
import { Modal } from 'antd';
import { GoVerified } from "react-icons/go";
import { verifyCustomer, saveApplication } from '../../service/customer.service';
import { Button, Link, Loading } from '@nextui-org/react';

function isJwtToken(str) {
  const regexExp = /^([a-zA-Z0-9_=]+)\.([a-zA-Z0-9_=]+)\.([a-zA-Z0-9_\-\+\/=]*)/gi;
  return regexExp.test(str);
};

const VerifyCustomer = () => {

  const [data, setData] = useState({});
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const [verified, setVerified] = useState(false);

  const router = useRouter();
  const { query: { token } } = router;


  useEffect(() => {
    (async () => {
      try {
        if(!isJwtToken){
          setError("Invalid Token !")
        }
        const decode = jwtDecode(token) || {};
        setData({
          firstName: decode.firstName,
          lastName: decode.lastName,
          email: decode.email
        });
        setError(null)
      }
      catch(err){
        console.log(err);
        setError("Invalid token !!!")
      }
    })()
  }, [token]);


  const handleVerify = async () => {
    setLoading(true);
    try {
      const { data: { data: newToken } } = await verifyCustomer({ verifyToken: token });
      const { id } = jwtDecode(newToken) || {};
      setTimeout(() => {
        setData({ ...data, id, token: newToken });
        setVerified(true);
        setLoading(false)
      }, 300);
    } catch (err) {
      setLoading(false)
      if (err?.response.data) {
        console.log(err);
      }
    }
  };


  const handleContinue = async () => {
    const { id } = data;
    const newData = { status: "Application Started" }
    // return console.log(id, newData);

    setLoading(true);

    try {
      await saveApplication(id, newData);
      setTimeout(() => {
        router.push(`/registration/${data.token}`)
        // setLoading(false);
      }, 300);
    } catch (err) {
      setLoading(false);
      if (err?.response.data) {
        console.log(err);
      }
    }
  };

  return (
    <div>
      <Modal
        width={600}
        footer={null}
        visible={true}
        closable={false}
        className='mt-5'
        maskClosable={false}
      >
        <div className='text-center'>
          {error ? (
            <div className='row'>
              <div className='text-center text-danger'>
                <h3>{error}</h3>
                <Link href="/" underline>Home</Link>
              </div>
            </div>
          )
          : verified ?
            <div className='row'>
              <div className='col-md-12 my-3'>
                <GoVerified color='#17c964' size={70} />
              </div>
              <div className='col-12 my-3'>
                <h3>Congratulations <span className='fw-bold'>{data?.firstName}!</span></h3>
                <h5>Your email has been verified. <br/>Click continue to complete your profile.</h5>
              </div>
              <div className='col-12 d-flex justify-content-center my-5'>
                <Button 
                  auto
                  bordered
                  className='ms-2'
                  color="secondary"
                  onClick={handleContinue}
                >
                  Continue
                </Button>
                {loading && <Loading className='m-2' type="spinner" />}
              </div>
            </div>
            :
            <div className='row'>
              <div className='col-12 my-5'>
                <h3>Hey <span className='fw-bold'>{data?.firstName}!</span></h3>
                <h5 className='mt-4'>Let's verify your email.</h5>
              </div>
              <div className='col-12 d-flex justify-content-center mb-5'>
                <Button 
                  auto
                  bordered
                  className='ms-2'
                  color="secondary"
                  onClick={handleVerify}
                >
                  Verify
                </Button>
                {loading && <Loading className='m-2' type="spinner" />}
              </div>
            </div>
          }
        </div>

      </Modal>
    </div>
  )
}

export default VerifyCustomer;