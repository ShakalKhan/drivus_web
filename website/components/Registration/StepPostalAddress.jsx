import React, { useState, useEffect } from 'react';
import { Form, InputNumber, Progress, Select, Upload } from 'antd'
import { Button, Image, Input, Loading } from "@nextui-org/react";
import { UploadOutlined } from '@ant-design/icons';
import { saveApplication } from '../../service/customer.service';

const StepPostalAddress = ({ current, data, setData, nextStep }) => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    const newData = { ...data.postalAddress }
    if (data.postalAddress?.filePath) {
      newData.fileList = [{ uid: 1, name: data.postalAddress.filePath, url: `http://localhost:5000/uploads/${data.postalAddress.filePath}` }]
    }
    form.setFieldsValue(newData);
  }, [current, data.id]);


  const handleFinish = async (values) => {

    const { street, suburb, state, postCode, documentType, filePath } = values;
    const newData = {
      postalAddress: { street, suburb, state, postCode, documentType, filePath }
    }
    // return console.log(values);

    if (!isModified(values)) return nextStep();

    try {
      setLoading(true);
      setTimeout(async () => {
        await saveApplication(data.id, newData);
        setLoading(false);
        setData({ ...data, ...newData });
        nextStep()
      }, 300);
    } catch (err) {
      setLoading(false);
      if (err.response?.data) {
        console.log(err.response.data.errors);
      }
    }
  }


  const isModified = (values) => {
    const { street, suburb, state, postCode, documentType, filePath } = data.postalAddress || {};
    if (street !== values.street || suburb !== values.suburb || state !== values.state || postCode !== values.postCode || documentType !== values.documentType || filePath !== values.filePath) return true;
    return false;
  }


  const normFile = (e) => {
    if (Array.isArray(e)) return e;
    const filePath = e.file?.response?.file[0]?.filename;
    form.setFieldsValue({ filePath })
    return e && e.fileList;
  };

  return (
    <div className='container'>
      <div className="row" style={{ minHeight: "650px" }}>
        <div className="col-md-7">
          <Form
            form={form}
            autoComplete="off"
            onFinish={handleFinish}
          >
            {values => (
              <div className="row">
                <div className="col-12 mt-5 pt-lg-5">
                  <h2>Provide your Postal address information</h2>
                </div>

                <div className="col-md-12 mt-4">
                  <Form.Item
                    name="street"
                    rules={[
                      {
                        required: true,
                        message: "Street name is required"
                      },
                    ]}
                  >
                    <Input
                      aria-label='street'
                      placeholder='Street Name'
                      underlined
                      clearable
                      color="primary"
                      style={{ width: "400px" }}
                    />
                  </Form.Item>

                  <Form.Item
                    name="suburb"
                    rules={[
                      {
                        required: true,
                        message: "Sub-Urban is required"
                      }
                    ]}
                  >
                    <Input
                      aria-label='Suburb'
                      placeholder='Sub-Urban'
                      underlined
                      clearable
                      color="primary"
                      style={{ width: "400px" }}
                    />
                  </Form.Item>

                  <Form.Item
                    name="state"
                    rules={[
                      {
                        required: true,
                        message: "State is required"
                      }
                    ]}
                  >
                    <Input
                      aria-label='State'
                      placeholder='State'
                      underlined
                      clearable
                      color="primary"
                      style={{ width: "400px" }}
                    />
                  </Form.Item>

                  <Form.Item
                    name="postCode"
                    rules={[
                      {
                        required: true,
                        message: "Postal Code is required"
                      }
                    ]}
                  >
                    <Input
                      aria-label='postCode'
                      placeholder='Postal Code'
                      underlined
                      clearable
                      color="primary"
                      style={{ width: "400px" }}
                    />
                  </Form.Item>

                  <div className='row'>
                    <div className='col-4'>
                      <Form.Item
                        name="documentType"
                        rules={[
                          {
                            required: true,
                            message: "Document Type is required"
                          }
                        ]}
                      >
                        <Select
                          size='large'
                          allowClear
                          placeholder='Upload Document'
                        >
                          <Select.Option value="Electricity Bill">Electricity Bill</Select.Option>
                          <Select.Option value="Gas Bill">Gas Bill</Select.Option>
                        </Select>
                      </Form.Item>
                    </div>

                    <div className="col-8">
                      <Form.Item
                        name="fileList"
                        valuePropName="fileList"
                        getValueFromEvent={normFile}
                        rules={[
                          {
                            required: true,
                            message: "File is required"
                          },
                        ]}
                      >
                        <Upload
                          name='file'
                          // listType='picture'
                          maxCount={1}
                          beforeUpload={() => true}
                          action={`http://localhost:5000/api/v1/files/uploads`}
                        >
                          {(values.fileList || []).length < 1 &&
                            <Button
                              bordered
                              auto
                              color="secondary"
                              size="sm"
                              icon={<UploadOutlined />}
                            > Upload</Button>
                          }
                        </Upload>
                      </Form.Item>

                      <Form.Item
                        name="filePath"
                        hidden
                      >
                        <InputNumber />
                      </Form.Item>

                    </div>
                  </div>
                </div>


                <div className="col-12 d-flex mt-5">
                  <Button color="secondary" bordered auto onClick={nextStep}>{`Skip >>`}</Button>
                  <Button
                    auto
                    className='ms-2'
                    color="secondary"
                    type="submit"
                  >{isModified(values) ? 'Save & Continue' : 'Continue'}</Button>
                  {loading && <Loading className='m-2' type="spinner" />}
                </div>
              </div>
            )}
          </Form>

        </div>
        <div className="col-md-5">
          <Image
            src="/img/maps.png"
            alt="Driving License"
            width={700}
            height={500}
          />
        </div>

      </div>

      <Progress
        strokeColor={{
          from: '#108ee9',
          to: '#87d068',
        }}
        percent={80}
        status="active"
      />
    </div>
  )
}

export default StepPostalAddress