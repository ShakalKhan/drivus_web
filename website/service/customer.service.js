import axios from 'axios'

const apiUrl = "http://localhost:5000/api/v1/customers"


export function saveSelfRegistration(data) {
  // if (data.id) return http.put(`${apiUrl}/${data.id}`, data)
  return axios.post(apiUrl, data)
}


export function getCustomer(id) {
  return axios.get(`${apiUrl}/${id}`)
};

export function saveCustomer(data) {
  if (data.id) return axios.put(`${apiUrl}/${data.id}`, data);
  return axios.post(apiUrl, data);
};

export function saveApplication(id, data) {
  console.log(id, data);
  return axios.patch(`${apiUrl}/application/${id}`, data);
};

export function verifyCustomer(data) {
  return axios.post(`${apiUrl}/verify`, data);
};

export function sendEmail(data) {
  return axios.post(`${apiUrl}/send-mail`, data);
};

// export function applicationStarted(data) {
//   return axios.post(`${apiUrl}/application-started`, data);
// };

export function findCustomerUser(data) {
  return axios.post(`${apiUrl}/find-customer-user`, data);
};