import { useEffect } from "react";

// global css
import '../styles/globals.css';
// antd
import 'antd/dist/antd.css';
// bootsrap
import 'bootstrap/dist/css/bootstrap.min.css';


function MyApp({ Component, pageProps }) {

  useEffect(() => {
    import('bootstrap/dist/js/bootstrap.bundle.min.js');
    // import('./main.js');
  }, []);

  return (
    <Component {...pageProps} />
  )
}

export default MyApp;
