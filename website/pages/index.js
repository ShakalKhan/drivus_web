import ClientSay from './client-say';
import How from './how';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';
import Header from '../components/Header';
import { BsGeoAlt, BsEnvelope, BsPhone } from "react-icons/bs";
import { BiHelpCircle, BiChevronDown, BiChevronUp } from "react-icons/bi";

const Home = () => {
  return (
    <div>
      <Header />
      <Navbar />
      <How />
      <ClientSay />
      <section id="faq" className="faq section-bg">
        <div className="container" data-aos="fade-up">

          <div className="section-title">

            <h2 className='fw-bold'>Frequently Asked Questions</h2>
            <p style={{ fontSize: 17 }}>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p>
          </div>

          <div className="accordion-list">
            <ul>
              <li data-aos="fade-up">
                <i className="icon-help"><BiHelpCircle /></i><a data-bs-toggle="collapse" className="collapse" style={{ fontSize: 17 }} data-bs-target="#accordion-list-1">Non consectetur a erat nam at lectus urna duis? <i className="icon-show"><BiChevronDown /></i><i className="icon-close"><BiChevronUp /></i></a>
                <div id="accordion-list-1" className="collapse show" data-bs-parent=".accordion-list">
                  <p style={{ fontSize: 17 }}>
                    Feugiat pretium nibh ipsum consequat. Tempus iaculis urna id volutpat lacus laoreet non curabitur gravida. Venenatis lectus magna fringilla urna porttitor rhoncus dolor purus non.
                  </p>
                </div>
              </li>

              <li data-aos="fade-up" data-aos-delay="100">
                <i className="icon-help"><BiHelpCircle /></i> <a data-bs-toggle="collapse" data-bs-target="#accordion-list-2" style={{ fontSize: 17 }} className="collapsed">Feugiat scelerisque varius morbi enim nunc? <i className="icon-show"><BiChevronDown /></i><i className="icon-close"><BiChevronUp /></i></a>
                <div id="accordion-list-2" className="collapse" data-bs-parent=".accordion-list">
                  <p style={{ fontSize: 17 }}>
                    Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Id interdum velit laoreet id donec ultrices. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Est pellentesque elit ullamcorper dignissim. Mauris ultrices eros in cursus turpis massa tincidunt dui.
                  </p>
                </div>
              </li>

              <li data-aos="fade-up" data-aos-delay="200">
                <i className="icon-help"><BiHelpCircle /></i> <a data-bs-toggle="collapse" data-bs-target="#accordion-list-3" style={{ fontSize: 17 }} className="collapsed">Dolor sit amet consectetur adipiscing elit? <i className="icon-show"><BiChevronDown /></i><i className="icon-close"><BiChevronUp /></i></a>
                <div id="accordion-list-3" className="collapse" data-bs-parent=".accordion-list">
                  <p style={{ fontSize: 17 }}>
                    Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis
                  </p>
                </div>
              </li>

              <li data-aos="fade-up" data-aos-delay="300">
                <i className="icon-help"><BiHelpCircle /></i> <a data-bs-toggle="collapse" data-bs-target="#accordion-list-4" style={{ fontSize: 17 }} className="collapsed">Tempus quam pellentesque nec nam aliquam sem et tortor consequat? <i className="icon-show"><BiChevronDown /></i><i className="icon-close"><BiChevronUp /></i></a>
                <div id="accordion-list-4" className="collapse" data-bs-parent=".accordion-list">
                  <p style={{ fontSize: 17 }}>
                    Molestie a iaculis at erat pellentesque adipiscing commodo. Dignissim suspendisse in est ante in. Nunc vel risus commodo viverra maecenas accumsan. Sit amet nisl suscipit adipiscing bibendum est. Purus gravida quis blandit turpis cursus in.
                  </p>
                </div>
              </li>

              <li data-aos="fade-up" data-aos-delay="400">
                <i className="icon-help"><BiHelpCircle /></i> <a data-bs-toggle="collapse" data-bs-target="#accordion-list-5" style={{ fontSize: 17 }} className="collapsed">Tortor vitae purus faucibus ornare. Varius vel pharetra vel turpis nunc eget lorem dolor? <i className="icon-show"><BiChevronDown /></i><i className="icon-close"><BiChevronUp /></i></a>
                <div id="accordion-list-5" className="collapse" data-bs-parent=".accordion-list">
                  <p style={{ fontSize: 17 }}>
                    Laoreet sit amet cursus sit amet dictum sit amet justo. Mauris vitae ultricies leo integer malesuada nunc vel. Tincidunt eget nullam non nisi est sit amet. Turpis nunc eget lorem dolor sed. Ut venenatis tellus in metus vulputate eu scelerisque.
                  </p>
                </div>
              </li>

            </ul>
          </div>

        </div>
      </section>

      <section id="contact" className="contact">
        <div className="container" data-aos="fade-up">

          <div className="section-title">
            <h2 className='fw-bold'>Contact Us</h2>
            <p style={{ fontSize: 17 }}>Contact us to get started</p>
          </div>

          <div className="row">

            <div className="col-lg-5 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
              <div className="info">
                <div className="address">
                  <i><BsGeoAlt /></i>
                  <h4>Location:</h4>
                  <p>A108 Adam Street, New York, NY 535022</p>
                </div>

                <div className="email">
                  <i><BsEnvelope /></i>
                  <h4>Email:</h4>
                  <p>info@example.com</p>
                </div>

                <div className="phone">
                  <i><BsPhone /></i>
                  <h4>Call:</h4>
                  <p>+1 5589 55488 55s</p>
                </div>

                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameBorder="0" style={{ border: 0, width: "100%", height: 290 }} allowFullScreen></iframe>
              </div>

            </div>

            <div className="col-lg-7 mt-5 mt-lg-0 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
              <form action="forms/contact.php" method="post" role="form" className="php-email-form">
                <div className="row">
                  <h4 className='text-center mb-4' style={{ color: "#272561" }}>Send us a Message</h4>
                  <div className="form-group col-md-6">
                    <label htmlFor="name">Your Name</label>
                    <input type="text" name="name" className="form-control" id="name" placeholder="Your Name" required />
                  </div>
                  <div className="form-group col-md-6 mt-3 mt-md-0">
                    <label htmlFor="name">Your Email</label>
                    <input type="email" className="form-control" name="email" id="email" placeholder="Your Email" required />
                  </div>
                </div>
                <div className="form-group mt-3">
                  <label htmlFor="name">Subject</label>
                  <input type="text" className="form-control" name="subject" id="subject" placeholder="Subject" required />
                </div>
                <div className="form-group mt-3">
                  <label htmlFor="name">Message</label>
                  <textarea className="form-control" name="message" rows="10" required></textarea>
                </div>
                <div className="my-3">
                  <div className="loading">Loading</div>
                  <div className="error-message"></div>
                  <div className="sent-message">Your message has been sent. Thank you!</div>
                </div>
                <div className="text-center"><button type="submit">Send Message</button></div>
              </form>
            </div>

          </div>

        </div>
      </section>
      <Footer />
    </div>
  )
};

export default Home;
