import React, { useState, useEffect } from 'react';
import { Form, Input, Progress, Select, Upload, Button as AntButton, InputNumber } from 'antd'
import { Button, Image, Loading } from "@nextui-org/react";
import { UploadOutlined, PlusOutlined, MinusCircleOutlined } from '@ant-design/icons';
import { saveApplication } from '../../service/customer.service';

const StepIncomeSrc = ({ current, data, setData, nextStep }) => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    const incomeSources = (data.incomeSources || [{}]).map((item, idx) => {
      const newItem = {};
      if (item.name) {
        newItem.preName = item.name;
        newItem.name = item.name;
      }
      if (item.amount) {
        newItem.preAmount = item.amount;
        newItem.amount = item.amount;
      }
      if (item.filePath) {
        newItem.preFilePath = item.filePath;
        newItem.filePath = item.filePath;
        newItem.fileList = [{ uid: `income_${idx}`, name: item.filePath, url: `http://localhost:5000/uploads/${item.filePath}` }]
      }
      return newItem;
    });

    form.setFieldsValue({ incomeSources });
  }, [current, data.id]);


  const normFile = (e, arrName, key) => {
    if (Array.isArray(e)) return e;
    const filePath = e.file?.response?.file && e.file.response.file[0]?.filename;
    // console.log("EEEEEE", filePath, key, e.file);
    const information = [...form.getFieldValue(arrName)];
    if (information[key]) {
      information[key].filePath = filePath;
    }
    form.setFieldsValue({ [arrName]: information })
    return e && e.fileList;
  };


  const handleFinish = async values => {

    if (!isModified(values)) nextStep();

    const newData = {
      incomeSources: values.incomeSources.map(({ name, amount, filePath }) => ({ name, amount, filePath }))
    }

    try {
      setLoading(true);
      setTimeout(async () => {
        await saveApplication(data.id, newData);
        setLoading(false);
        setData({ ...data, ...newData });
        nextStep()
      }, 300);
    } catch (err) {
      setLoading(false);
      if (err.response?.data) {
        console.log(err.response.data.errors);
      }
    }
  }


  const isModified = values => {
    // thik korte hobe
    let truth = false
    if (data.incomeSources?.length !== values.incomeSources?.length) return true;
    values.incomeSources?.forEach(({ name, preName, amount, preAmount, filePath, preFilePath }) => {
      if (name !== preName || amount !== preAmount || filePath !== preFilePath) return truth = true;
    })
    return truth;
  }


  return (
    <div className='container'>
      <div className="row" style={{ minHeight: "650px" }}>
        <div className="col-md-7">
          <Form
            form={form}
            name="formData"
            autoComplete="off"
            onFinish={handleFinish}
          >
            {
              (values) => (
                <div className="row">
                  <div className="col-12 mt-5 mb-4 pt-lg-5">
                    <h2>Provide your Income Source Information</h2>
                  </div>

                  <div className="col-md-12">
                    <h6 className='text-primary'>Source of Income</h6>
                    <Form.List name="incomeSources">
                      {(fields, { add, remove }) => (
                        <>
                          {fields.map(({ key, name }) => (
                            <div className='row' key={key}>
                              <div className="col-4">
                                <Form.Item
                                  name={[name, 'name']}
                                  rules={[{ required: true, message: 'Name is required' }]}
                                >
                                  <Select
                                    allowClear
                                    placeholder='Select Income Source'
                                  >
                                    <Select.Option key="1" value="Job">Job</Select.Option>
                                    <Select.Option key="2" value="Business">Business</Select.Option>
                                  </Select>
                                </Form.Item>
                              </div>

                              <div className='col-3'>
                                <Form.Item
                                  name={[name, 'amount']}
                                  rules={[{ required: true, message: 'Amount is required' }]}
                                >
                                  <InputNumber
                                    placeholder='Enter Amount'
                                    className='w-100' />
                                </Form.Item>
                              </div>

                              <>
                                <Form.Item
                                  name={[name, 'preName']}
                                  hidden
                                >
                                  <Input />
                                </Form.Item>

                                <Form.Item
                                  name={[name, 'preAmount']}
                                  hidden
                                >
                                  <Input />
                                </Form.Item>

                                <Form.Item
                                  name={[name, 'preFilePath']}
                                  hidden
                                >
                                  <Input />
                                </Form.Item>

                                <Form.Item
                                  name={[name, 'filePath']}
                                  hidden
                                >
                                  <Input />
                                </Form.Item>
                              </>

                              <div className="col-4">
                                <Form.Item
                                  name={[name, 'fileList']}
                                  valuePropName="fileList"
                                  getValueFromEvent={(e) => normFile(e, 'incomeSources', name)}
                                  rules={[{ required: true, message: "File is required" }]}
                                >
                                  <Upload
                                    name='file'
                                    // fileList={formData.drivingLicense ? (formData.drivingLicense.filePath||[]) : []}
                                    // listType='picture'
                                    maxCount={1}
                                    beforeUpload={() => true}
                                    action={`http://localhost:5000/api/v1/files/uploads`}
                                  >
                                    {(values.incomeSources[name]?.fileList || []).length < 1 &&
                                      <AntButton icon={<UploadOutlined />}>Upload</AntButton>
                                    }
                                  </Upload>
                                </Form.Item>
                              </div>
                              <div className="col-1">
                                {
                                  values.incomeSources.length > 1 ?
                                    <MinusCircleOutlined className='delete' onClick={() => remove(name)} />
                                    : <MinusCircleOutlined className='text-muted' />
                                }
                              </div>
                            </div>
                          ))}
                          <Form.Item>
                            <Button
                              color=""
                              size="sm"
                              auto
                              bordered
                              onClick={() => add({})}
                            ><PlusOutlined className='me-2' /> Add</Button>
                          </Form.Item>
                        </>
                      )}
                    </Form.List>
                  </div>


                  <div className="col-8 d-flex my-5">
                    <Button color="secondary" bordered auto onClick={nextStep}>{`Skip >>`}</Button>
                    <Button
                      auto
                      className='ms-2'
                      color="secondary"
                      type="submit"
                    >{isModified(values) ? 'Save & Continue' : 'Continue'}</Button>
                    {loading && <Loading className='m-2' type="spinner" />}
                  </div>
                </div>
              )
            }
          </Form>

        </div>
        <div className="col-md-5">
          <div className="row mt-5 pt-lg-5">

            <div className="col-md-11 ">
              <Image
                src="/img/documents.png"
                alt="Driving License"
                width={500}
                height={300}
              />
            </div>
          </div>
        </div>

      </div>

      <Progress
        strokeColor={{
          from: '#108ee9',
          to: '#87d068',
        }}
        percent={50}
        status="active"
        className='mb-5'
      />
    </div>
  )
}

export default StepIncomeSrc