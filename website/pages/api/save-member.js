import axios from 'axios'


const apiUrl = 'http://localhost:3900/api/v1/customers/verify'

export function saveMember(data) {
  if (data.id) return http.put(`${apiUrl}/${data.id}`, data)
  return axios.post(apiUrl, data);
}