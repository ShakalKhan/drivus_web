import React from 'react'
import Image from 'next/image'
import styles from '../styles/Testimonial.module.css'
import journey1 from '../public/journey1.jpg'
import journey2 from '../public/journey2.jpg'
import 'animate.css';
import Header from '../components/Header';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';

const AboutUs = () => {
  return (
    <div>
      <Header />
      <Navbar />
      <div className={`${styles.vision} animate__animated animate__fadeIn animate__slow`}>
        <div className='container-fluid' style={{ height: 520 }}>

          <div className='row'>

            <div className='col-md-2'></div>

            <div className='col-md-4 mt-4 pt-5 animate__animated animate__fadeInUp animate__delay-2s'>
              <h3 className='fw-bold' style={{ fontSize: 27, color: "#272561" }}>Our Vision</h3>
              <h1 className='fw-bold mt-3' style={{ color: "#272561", fontSize: 40 }}>A future where everyone can <br /><span className='text-danger'>own their car.</span></h1>
            </div>

            <div className='col-md-5'></div>

          </div>
        </div>
      </div>

      <div className='container mt-5 animate__animated animate__fadeInUp animate__delay-2s'>

        <div className='mt-5'>
          <h2 className='fw-bold' style={{ fontSize: "35", color: "#272561" }}>Principles that <span style={{ color: "#FF0000" }}>D</span>rive us!</h2>
        </div>
      </div>

      <section id="services" className="services animate__animated animate__fadeInUp animate__delay-2s">
        <div className="container">

          <div className="section-title">
            <h2 className='fw-bold'>Services</h2>
            <p style={{ fontSize: 17 }}>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint
              consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit
              in iste officiis commodi quidem hic quas.</p>
          </div>

          <div className="row">
            <div className="col-lg-4 col-md-6 d-flex align-items-stretch" style={{ cursor: "pointer" }}>
              <div className="icon-box">
                <div className="icon"><i className="fas fa-heartbeat"></i></div>
                <h4 className='fw-bold'><a href="">Refreshing frankness.</a></h4>
                <p style={{ fontSize: 17 }}>We cut through the sales chatter and tell people how it is. From our comms to our customer service, we are open, candid, and
                  down-to-earth. We take time to empathies with our members, so everything we say and do stays relevant.
                </p>
              </div>
            </div>

            <div className="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-md-0" style={{ cursor: "pointer" }}>
              <div className="icon-box">
                <div className="icon"><i className="fas fa-pills"></i></div>
                <h4 className='fw-bold'><a href="">Proactive optimism.</a></h4>
                <p style={{ fontSize: 17 }}>We are motivated on behalf of our members.We are naturally positive and focus on solutions rather than fixating on problems. We are all
                  about getting things done, in the best way possible.
                </p>
              </div>
            </div>

            <div className="col-lg-4 col-md-6 d-flex align-items-stretch mt-4 mt-lg-0" style={{ cursor: "pointer" }}>
              <div className="icon-box">
                <div className="icon"><i className="fas fa-hospital-user"></i></div>
                <h4 className='fw-bold'><a href="">Knowledgeable support.</a></h4>
                <p style={{ fontSize: 17 }}>We care about what we do and have the knowledge and data analytics to be the best at it. We are approachable experts and apply our
                  skills for the benefit of others. Our supportive team is always ready to help.
                </p>
              </div>
            </div>

          </div>

        </div>
      </section>

      <div className='bg-light'>
        <div className='container'>
          <h1 className='fw-bold pt-5' style={{ color: "#272561", fontSize: 32 }}>Our journey</h1>
          <div className='row flex-md-row flex-column-reverse'>

            <div className='col-md-6' style={{ color: "#25225B", fontSize: 17 }}>
              <p className='mt-4'>Drivus provides eco-friendly and affordable cars to on-demand drivers, so they can focus on earning an income.</p>
              <p className='mt-4'>For over five years, we have been helping drivers earn more, improve their ratings, and enjoy their jobs. How do we do that? By offering
                an unrivalled level of support for our members.
              </p>

              <p className='mt-4'>We are more than a car supplier. We provide resources and a community for drivers wanting to be their best. From dedicated
                support to advanced training and rewards, we help our members take control of their careers with our flexible plans.
              </p>

            </div>

            <div className='col-md-6'>
              {/* Image Jabey */}
              <Image src={journey1} alt="journey" />
            </div>
          </div>

          <div className='row mt-5'>
            <div className='col-md-4'>
              {/* image jabey */}
              <Image src={journey2} alt="travel" />
            </div>

            <div className='col-md-8' style={{ color: "#25225B", fontSize: 17 }}>
              <p>Drivus was founded in Sydney in 2015. As on-demand driving became part of modern society, we expanded to seven cities across Australia, and Europe, grew our global team to over 100
                employees, and raised $25 million to date.
              </p>
              <p className='mt-4 mb-5 pb-5'>We have helped students, retirees, former refugees, and single parents find work they love, which fits their lifestyle. We take pride
                in everything our members and teams achieved so far, and we are only just getting started.
              </p>
            </div>
          </div>

        </div>

      </div>

      <div className='container'>

        <iframe className='mt-5 mb-4' style={{ width: "100%", height: "590px", borderRadius: 16 }} src="https://www.youtube.com/embed/8LSt8_11wbQ" title="YouTube video player" frameBorder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
      </div>

      <Footer />
    </div>
  )
}

export default AboutUs;