import React, { useState, useEffect } from 'react';
import { Form, InputNumber, Progress, Radio } from 'antd';
import { Button, Image, Input, Loading } from "@nextui-org/react";
import { saveApplication, saveCustomer } from '../../service/customer.service';

const SetpCitizen = ({ current, data, setData, nextStep }) => {
  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    // console.log("data", data.isAgree);
    form.setFieldsValue({ citizenship: data.citizenship, residency: data.residency })
  }, [current, data.id]);

  const handleFinish = async (values) => {
    const { citizenship, residency } = values;
    const newData =  { citizenship, residency } 
    // return console.log("data", values, data.citizenship, data.residency);
    
    if (!isModified(newData)) return nextStep();
    
    try {
      setLoading(true);
      setTimeout(async () => {
        await saveApplication(data.id, newData);
        setLoading(false);
        setData({ ...data, ...newData });
        nextStep()
      }, 300);
    } catch (err) {
      setLoading(false);
      if (err.response?.data) {
        console.log(err.response.data.errors);
      }
    }
  }

  const isModified = (values) => {
    if (data.citizenship !== values.citizenship) return true;
    if (data.citizenship === "Other" && data.residency !== values.residency) return true;
    return false;
  }

  return (
    <div className='container'>
      <div className='row' style={{ minHeight: "650px" }}>
        <div className='col-md-7'>
          <Form
            form={form}
            autoComplete="off"
            onFinish={handleFinish}
          >
            {values => (
              <div className="row mt-5">
                <div className='col-12 my-5 pt-lg-5'>
                  <h2>Please choose the country of your citizenship or permanent residency.</h2>
                </div>
                <div className="col-12 mb-5">
                  <Form.Item
                    name="citizenship"
                    rules={[
                      {
                        required: true,
                        message: 'Please provide Citizenship or residency',
                      },
                    ]}
                  >
                    <Radio.Group
                      size="large"
                    >
                      <Radio.Button value="AUS">AUS</Radio.Button>
                      <Radio.Button value="NZ">NZ</Radio.Button>
                      <Radio.Button value="Other">Other Residency</Radio.Button>
                    </Radio.Group>
                  </Form.Item>
                </div>
                {values.citizenship === 'Other' &&
                  <div className='col-12 mb-4'>
                    <Form.Item
                      name="residency"
                      rules={[
                        {
                          required: true,
                          message: 'Residency is required',
                        },
                      ]}
                    >
                      <Input
                        size='lg'
                        clearable
                        underlined
                        color="primary"
                        labelPlaceholder="Enter your Residency"
                        style={{ width: "400px" }}
                      />
                    </Form.Item>
                  </div>
                }
                <div className="col-12 d-flex">
                  <Button color="secondary" bordered auto onClick={nextStep}>{`Skip >>`}</Button>
                  <Button
                    auto
                    className='ms-2'
                    color="secondary"
                    type="submit"
                  >{isModified(values) ? 'Save & Continue' : 'Continue'}</Button>
                  {loading && <Loading className='m-2' type="spinner" />}
                </div>

              </div>
            )}
          </Form>
        </div>
        <div className='col-md-5'>
          <div className='mt-5'>
            <Image
              src="/img/citizenship.png"
              alt="Driving License"
              width={700}
            />
          </div>
        </div>
      </div>

      <div className='mt-5'>
        <Progress
          strokeColor={{
            from: '#108ee9',
            to: '#87d068',
          }}
          percent={10}
          status="active"
        />
      </div>

    </div>
  )
}

export default SetpCitizen;