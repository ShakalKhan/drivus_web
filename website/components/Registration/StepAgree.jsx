import React, { useState, useEffect } from 'react';
import { Form, Progress } from 'antd';
import { Button, Checkbox, Image, Loading } from "@nextui-org/react";
import { saveApplication } from '../../service/customer.service';

const StepAgree = ({ current, data, setData, nextStep }) => {

  const [form] = Form.useForm();
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    // console.log("data", data.isAgree);
    if (data.isAgree) {
      form.setFieldsValue({ isAgree: data.isAgree })
    }
  }, [current, data.id]);


  const handleFinish = async (values) => {

    const newData = { isAgree: values.isAgree }

    try {
      if (data.isAgree === newData.isAgree) return nextStep();
      setLoading(true);
      setTimeout(async () => {
        await saveApplication(data.id, newData);
        setLoading(false);
        setData({ ...data, ...newData });
        nextStep()
      }, 300);
    } catch (err) {
      setLoading(false);
      if (err.response?.data) {
        console.log(err.response.data.errors);
      }
    }
  }


  return (
    <div className="container">
      <div className='row' style={{ minHeight: "650px" }}>
        <div className="col-md-7">
          <Form
            form={form}
            autoComplete="off"
            onFinish={handleFinish}
          >
            {values => (
              <div className="row">
                <div className='col-12 mt-5 pt-lg-5'>
                  <h2>By checking the box below you agree with Drivus, that you do not intend to use any of Drivus vehicles for any kind of ride sharing app.</h2>
                </div>
                <div className="col-12 my-5">
                  <Form.Item
                    name="isAgree"
                    valuePropName="checked"
                    rules={[
                      {
                        validator: (_, value) =>
                          value ? Promise.resolve() : Promise.reject(new Error('Should accept agreement')),
                      }
                    ]}
                  >
                    <Checkbox
                      className={`w-75 p-3 rounded border ${values.isAgree && "border-primary"} ride`}
                    >Agree</Checkbox>
                  </Form.Item>
                </div>
                <div className="col-12 d-flex">
                  <Button color="secondary" bordered auto onClick={nextStep}>{`Skip >>`}</Button>
                  <Button
                    auto
                    className='ms-2'
                    color="secondary"
                    type="submit"
                  >{(!!data.isAgree === !!values.isAgree) ? 'Continue' : 'Save & Continue'}</Button>
                  {loading && <Loading className='m-2' type="spinner" />}
                </div>
              </div>
            )}
          </Form>
        </div>

        <div className="col-5">
          <Image
            src="/img/agree.png"
            alt="Driving License"
            width={700}
          />
        </div>
      </div>

      <div className='mt-5'>
        <Progress
          strokeColor={{
            from: '#108ee9',
            to: '#87d068',
          }}
          percent={20}
          status="active"
        />
      </div>

    </div>
  )
}

export default StepAgree;